#!/bin/bash

TMP_DIR=/tmp/check_tarball

if [ $# != 1 ]; then
    echo "$0 <cts_tarball.tgz>"
    exit 1
fi

cts_tarball_path=$(realpath $1)
cts_tarball_name=$(basename $1)

set -ex

# Cleanup previous checks.
rm -rf $TMP_DIR

mkdir -p $TMP_DIR/tarball

# Grab the verify submission tool (require Khronos member access).
git clone git@gitlab.khronos.org:Tracker/vk-gl-cts-ci.git $TMP_DIR/vk-gl-cts-ci

# XXX: Apply a fix from VK-GL-CTS-Tools for the mustpass list.
patch=`pwd`"/0001-Updates-mustpass-reading-for-new-format.patch"
pushd $TMP_DIR/vk-gl-cts-ci/
git am $patch
popd

# Grab VK-GL-CTS which is used by the verify submission tool.
git clone https://github.com/KhronosGroup/VK-GL-CTS.git $TMP_DIR/VK-GL-CTS

# Prefix the CTS tarball with an arbitrary submission number.
cp $cts_tarball_path $TMP_DIR/42-$cts_tarball_name

# Run the verify submission tool.
cd $TMP_DIR/vk-gl-cts-ci/verify
python2.7 verify_submission.py $TMP_DIR/42-$cts_tarball_name -d $TMP_DIR/tarball -s $TMP_DIR/VK-GL-CTS -v
