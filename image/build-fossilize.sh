#!/bin/bash

set -ex

FOSSILIZE_VERSION="4c4348f"

git clone https://github.com/ValveSoftware/Fossilize.git /Fossilize
pushd /Fossilize
git checkout $FOSSILIZE_VERSION

git submodule update --init

cmake -Bbuild -DCMAKE_BUILD_TYPE=Release -GNinja .
ninja -C build install
popd
rm -rf /Fossilize
