#!/bin/bash

set -ex

LLVM_VERSION="release/13.x"

git clone \
    --depth 1 \
    https://github.com/llvm/llvm-project.git \
    -b $LLVM_VERSION \
    /llvm-project
pushd /llvm-project

pushd llvm
cmake -B build -G Ninja \
   -DCMAKE_BUILD_TYPE=Release \
   -DLLVM_TARGETS_TO_BUILD="AMDGPU" \
   -DLLVM_OPTIMIZED_TABLEGEN=ON \
   -DLLVM_BUILD_LLVM_DYLIB=ON \
   -DLLVM_LINK_LLVM_DYLIB=ON \
   -DLLVM_INCLUDE_EXAMPLES=OFF
ninja -C build install
popd

rm -rf /llvm-project
popd
