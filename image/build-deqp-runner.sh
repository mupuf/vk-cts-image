#!/bin/bash
## Source: https://gitlab.freedesktop.org/mesa/mesa/-/blob/main/.gitlab-ci/container/build-deqp-runner.sh

set -ex

cargo install --locked deqp-runner \
  -j ${FDO_CI_CONCURRENT:-4} \
  --version 0.12.0 \
  --root /usr/local \
  $EXTRA_CARGO_ARGS
