#!/bin/bash

set -ex

VKD3D_PROTON_VERSION="3e5aab6f"

git clone https://github.com/HansKristian-Work/vkd3d-proton.git /vkd3d-proton
pushd /vkd3d-proton
git checkout $VKD3D_PROTON_VERSION
git submodule update --init --recursive
meson build -Denable_tests=true
ninja -C build
popd
