#!/bin/bash

set -ex

PIGLIT_VERSION="445711587d461539a4d8f9d35a7fe996a86d3c8d"

git clone https://gitlab.freedesktop.org/mesa/piglit.git --single-branch --no-checkout /piglit
pushd /piglit
git checkout $PIGLIT_VERSION
cmake -S . -B . -G Ninja -DCMAKE_BUILD_TYPE=Release
ninja
find -name .git -o -name '*ninja*' -o -iname '*cmake*' -o -name '*.[chao]' | xargs rm -rf
rm -rf target_api
popd
