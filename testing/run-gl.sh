#!/bin/bash

set -ex

RESULTS_DIR="/mnt/results"

/mnt/testing/build-mesa.sh
/mnt/testing/check-device.sh

pushd /mnt/mesa

set +e

# For piglit/dEQP.
export FDO_CI_CONCURRENT=$NUM_THREADS
export DEQP_SURFACE_TYPE="window"

# Copy the skips/fails/flakes lists.
cp .gitlab-ci/all-skips.txt install/
if [ "$GALLIUM_DRIVERS" == "zink" ]; then
    cp src/gallium/drivers/zink/ci/*radv* install/
fi

# Piglit
if [ "$RUN_PIGLIT" -eq 1 ]; then
    export PIGLIT_PROFILES="all"
    export PIGLIT_RESULTS_DIR="../.."$RESULTS_DIR"/piglit"
    /mnt/testing/piglit-runner.sh
fi

# dEQP (GLES2, GLES3, GLES31)
if [ "$RUN_DEQP" -eq 1 ]; then
    export DEQP_RESULTS_DIR="../.."$RESULTS_DIR"/deqp"
    /mnt/testing/deqp-runner.sh
fi

# dEQP (GLCTS 4.6)
if [ "$RUN_GLCTS" -eq 1 ]; then
    # TODO: add GL46 to the suite.
    unset DEQP_SUITE

    # GLCTS
    export DEQP_VER="gl46"
    export DEQP_RESULTS_DIR="../.."$RESULTS_DIR"/glcts"
    /mnt/testing/deqp-runner.sh
fi

popd
