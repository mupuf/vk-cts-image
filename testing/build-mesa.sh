#!/bin/bash

set -ex

pushd /mnt/mesa

# If a previous rebase failed for some reasons, make sure to abort it.
if [ -d .git/rebase-apply ]; then
    rm -rf .git/rebase-apply
fi

# Clean up the repo.
git clean -fdx .
git reset --hard
git checkout main

# Add the source remote if unknown.
set +e
remote=`git remote | grep -w $MESA_SOURCE_REMOTE`
set -ex
if [ -z $remote ]; then
    git remote add $MESA_SOURCE_REMOTE https://gitlab.freedesktop.org/$MESA_SOURCE_REMOTE/mesa.git
fi

# Fetch the source remote and checkout the branch.
git fetch $MESA_SOURCE_REMOTE

if [[ -z "${MESA_SOURCE_BRANCH}" ]]; then
    # Checkout the source commit.
    git checkout $MESA_SOURCE_COMMIT
else
    # Checkout the remote branch.
    git checkout $MESA_SOURCE_REMOTE/$MESA_SOURCE_BRANCH
fi

# Checkout the commit HEAD-n (0 is the default and the top commit).
git checkout HEAD~$MESA_SOURCE_HEAD

# Rebase the source branch on the origin/main branch if requested.
if [ $MESA_REBASE_BRANCH -eq 1 ]; then
    git fetch origin
    if ! git rebase origin/main; then
        git rebase --abort
        exit 1
    fi
fi

# Build the branch.
meson _build \
      -D prefix=`pwd`/install \
      -D libdir=lib \
      -D buildtype=$MESA_BUILDTYPE \
      -D platforms=x11 \
      -D dri-drivers= \
      -D gallium-drivers=${GALLIUM_DRIVERS:-} \
      -D vulkan-drivers=amd \
      -D build-aco-tests=true \
      -D build-tests=false \
      -D werror=true \
      -D libunwind=false \
      -D cpp_rtti=false

cd _build
meson configure

ninja -j $NUM_THREADS
LC_ALL=C.UTF-8 meson test --num-processes $NUM_THREADS
ninja install
cd ..

popd
