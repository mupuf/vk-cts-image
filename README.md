Vulkan CTS image
================

Easy to use docker container for running the CTS and fossilize with RADV.

Usage examples
--------------

By default, run-radv.sh runs CTS and vkd3d-proton against the requested Mesa
and GPU.

Running the CTS against upstream mesa on 12 threads with a Fiji GPU (such as R9 Nano):

```
./run-radv.sh -r origin -b main -g fiji -j 12 --no-vkd3d-proton
```

On a multi-gpu system, use `-d` to select the ID of the GPU to use.
This will be also checked against the GPU family specified by the `-g` option:

```
./run-radv.sh -r origin -b main -g fiji -d 1 -j 13 --no-vkd3d-proton
```

Running the CTS against someone's custom branch on the Freedesktop gitlab:

```
./run-radv.sh -r fdo_username -b your_feature_branch -g fiji -j 12 --no-vkd3d-proton
```

To running the CTS against a specific commit, use `-s <commit_hash>`:

```
./run-radv.sh -r origin -s commit_hash -g fiji -j 12 --no-vkd3d-proton
```

External sources
----------------

Some external sources are required, especially a mesa local copy which should
be cloned the first time.

```
./external/fetch_sources
```

Required dependencies
---------------------

* docker
* git-lfs

Various issues
--------------

### Permission denied

Shouldn't happen anymore, but if it does, disabling SELinux helps:

```
setenforce 0
```

### Docker fails to start properly with systemd

Might happen on older systemd and/or docker releases.
The following kernel parameter will workaround it:

```
systemd.unified_cgroup_hierarchy=0
```
