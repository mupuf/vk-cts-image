#!/bin/bash

TEST_IMAGE=$(cat IMAGE_VERSION)

result_dir=`pwd`/results
compiler="aco"
mesa_rebase_branch=1
mesa_build_type=debugoptimized
gpu_device_id=0
run_piglit=1
run_deqp=1
run_glcts=1
gl_driver=""

while getopts "hb:c:d:g:j:o:r:s:-:" OPTION; do
    case $OPTION in
    -)
        case "${OPTARG}" in
            zink)
                gl_driver="zink"
                ;;
            no-rebase)
                mesa_rebase_branch=0
                ;;
            no-piglit)
                run_piglit=0
                ;;
            no-deqp)
                run_deqp=0
                ;;
            no-glcts)
                run_glcts=0
                ;;
            *)
                if [ "$OPTERR" = 1 ] && [ "${optspec:0:1}" != ":" ]; then
                    echo "Unknown option --${OPTARG}" >&2
                fi
                exit 0
                ;;
        esac;;
    h)
        echo -n "$0 -r <source_remote> -b <source_branch> -g <gpu_family> "
        echo "[ -j <num_threads> -c <aco|llvm> -o <result_dir> --no-rebase ]"
        exit 0
        ;;
    b)
        mesa_branch=$OPTARG
        ;;
    c)
        compiler=$OPTARG
        ;;
    d)
        gpu_device_id=$OPTARG
        ;;
    g)
        gpu_family=$OPTARG
        ;;
    j)
        num_threads=$OPTARG
        ;;
    o)
        result_dir=$OPTARG
        ;;
    r)
        mesa_remote=$OPTARG
        ;;
    s)
        mesa_commit=$OPTARG
        ;;
    *)
        if [ "$OPTERR" != 1 ] || [ "${optspec:0:1}" = ":" ]; then
            echo "Non-option argument: '-${OPTARG}'" >&2
        fi
        exit 1
        ;;
    esac
done

if [ -z $mesa_remote ] || ([ -z $mesa_branch ] && [ -z $mesa_commit ]) || [ -z $gpu_family ]; then
    echo "To use with a branch:"
    echo "$0 -r <mesa_remote> -b <mesa_branch> -g <gpu_family> [-d <gpu_device_id>] [-j <deqp_parallel>] [-d <result_dir>] [-c <compiler>]"
    echo "To use with a commit:"
    echo "$0 -r <mesa_remote> -s <mesa_commit> -g <gpu_family> [-d <gpu_device_id>] [-j <deqp_parallel>] [-d <result_dir>] [-c <compiler>]"
    exit 1
fi

if [ "$compiler" != "aco" ] && [ "$compiler" != "llvm" ]; then
    echo "Invalid compiler option (accepted values are: aco, llvm)"
    exit 1
fi

# Enable LLVM via RADV_DEBUG if set.
radv_debug=""
if [ "$compiler" == "llvm" ]; then
    radv_debug="llvm,checkir"
fi

if [ -z $num_threads ]; then
    num_threads=$(nproc)
fi

set -ex

mkdir -p $result_dir

# Adjust the permissions of the X server host for docker.
xhost +local:root || exit 1

# Pull the latest image.
docker pull $TEST_IMAGE

# Build a specific Mesa remote/branch and run the testsuites.
docker run --rm \
    --device /dev/dri/renderD$((128+$gpu_device_id)) \
    --mount src=`pwd`/testing,target=/mnt/testing,type=bind \
    --mount src=`pwd`/external/mesa,target=/mnt/mesa,type=bind \
    --mount src=$result_dir,target=/mnt/results,type=bind \
    --network host \
    --security-opt label:disable \
    --volume /tmp/.X11-unix:/tmp/.X11-unix \
    --env DISPLAY=$DISPLAY \
    --env MESA_SOURCE_REMOTE=$mesa_remote \
    --env MESA_SOURCE_BRANCH=$mesa_branch \
    --env MESA_SOURCE_HEAD=0 \
    --env MESA_SOURCE_COMMIT=$mesa_commit \
    --env MESA_REBASE_BRANCH=$mesa_rebase_branch \
    --env MESA_BUILDTYPE=$mesa_build_type \
    --env NUM_THREADS=$num_threads \
    --env EXPECTED_GPU_FAMILY=$gpu_family \
    --env RADV_DEBUG=$radv_debug \
    --env RUN_PIGLIT=$run_piglit \
    --env RUN_DEQP=$run_deqp \
    --env RUN_GLCTS=$run_glcts \
    --env-file env \
    --env-file $gl_driver-env \
    -it $TEST_IMAGE \
    bin/bash /mnt/testing/run-gl.sh
